const express = require('express')
const app = express()
const port = 3000
const books = [
  {
    id: 1,
    author: "JK Rowling",
    title: "Harry Potter and Sorcerers Stone"
  }
]

app.use(express.json())

app.use((req, res, next) => {
  console.log("Ini request masuk")
  next()
})

app.get('/', (req, res) => {
  res.json({
    message: 'Hello World!'
  })
})

app.get('/books', (req, res) => {
  res.status(200).json(books)
})

app.post('/books', (req, res) => {
  const book = {
    id: (books[books.length - 1].id || 0) + 1,
    author: req.body.author,
    title: req.body.title
  }

  books.push(book)
  res.status(200).json(books)
})

app.get('/books/:id', (req, res) => {
  const id = req.params.id
  const targetBook = books.find(book => book.id == id)
  res.status(200).json(targetBook)
})

app.put('/books/:id', (req, res) => {
  const id = req.params.id
  const idxBook = books.findIndex(book => book.id == id)
  
  const book = {
    id: +id,
    author: req.body.author,
    title: req.body.title
  }

  books[idxBook] = book
  res.status(200).json(books)
})

app.delete('/books/:id', (req, res) => {
  const id = req.params.id
  const idxBook = books.findIndex(book => book.id == id)
  books.splice(idxBook, 1)
  res.status(200).json(books)
})

app.get('/about', (req, res) => {
  res.json({
    message: 'Ini info server'
  })
})

app.post('/sum', (req, res) => {
  res.json({
    parameters: req.body,
    operations: "sum",
    result: req.body.x + req.body.y
  })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})